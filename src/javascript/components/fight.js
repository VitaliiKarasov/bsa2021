import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const statusInfo = {
      block: false,  
      currentHealth: 100,
      timeOfCrit: Date.now(),
      criticalHit: []
    }
    const playerOne = { 
      ...firstFighter, 
      ...statusInfo, 
      healthBar: healthBars[0], 
      statusView: statusViews[0],
      position: 'left'
    }

    const playerTwo = { 
      ...secondFighter, 
      ...statusInfo, 
      healthBar: healthBars[1], 
      statusView: statusViews[1],
      position: 'right'
    }

    function showStatus(fighter, text) {
      if(document.getElementById(`${fighter.position}-status-marker`)) {
        document.getElementById(`${fighter.position}-status-marker`).remove();
      }

    const statusMarker = createCard({ tagName: 'div', className: 'arena___status-marker', attributes: {id: `${fighter.position}-status-marker`} });
      statusMarker.innerText = text;
      statusMarker.style.opacity = '1';
      fighter.statusView.append(statusMarker);
      setInterval(() => {
        if(statusMarker.style.opacity > 0) {
          statusMarker.style.opacity = statusMarker.style.opacity - 0.01;
        } else {
          statusMarker.remove();
        }
      }, 20);
    }

    function fightProcess(attacker, defender) {
      if(attacker.block) {
        return void 0;
      }

      if(defender.block) {
        return void 0;
      }

      const totalDamage = getDamage(attacker, defender);

      if(!totalDamage) {
        return void 0;
      }

      if(attacker.criticalHit.length === 3) {
        showStatus(attacker, 'Critical hit!');
      }

      showStatus(defender, `-${totalDamage.toFixed(1)}`);
      defender.currentHealth = defender.currentHealth - totalDamage / defender.health * 100;
      if(defender.currentHealth < 0) {
        document.removeEventListener('keydown', onDown);
        document.removeEventListener('keyup', onUp);
        resolve(attacker);
      }

      defender.healthBar.style.width = `${defender.currentHealth}%`;
    }

    

    function onDown(event) {
      if(!event.repeat) {
        switch(event.code) {
          case controls.PlayerOneAttack: {
            fightProcess(playerOne, playerTwo);
            break;
          }

          case controls.PlayerTwoAttack: {
            fightProcess(playerTwo, playerOne);
            break;
          }

          case controls.PlayerOneBlock: {
            playerOne.block = true;
            break;
          }

          case controls.PlayerTwoBlock: {
            playerTwo.block = true;
            break;
          }
        }
      }
    }

    function onUp(event) {
      switch(event.code) {
        case controls.PlayerOneBlock: playerOne.block = false; break;
        case controls.PlayerTwoBlock: playerTwo.block = false; break;
      }

      if(playerOne.criticalHit.includes(event.code)) {
        playerOne.criticalHit.splice(playerOne.criticalHit.indexOf(event.code), 1);
      }

      if(playerTwo.criticalHit.includes(event.code)) {
        playerTwo.criticalHit.splice(playerTwo.criticalHit.indexOf(event.code), 1);
      }
    }

    document.addEventListener('keydown', onDown);
    document.addEventListener('keyup', onUp)
  });
}
export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = fighter.criticalHit === 3 ? 2 : Math.random()+1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random()+1;
  return fighter.defense * dodgeChance;
}
