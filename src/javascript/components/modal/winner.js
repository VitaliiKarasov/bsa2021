import {showModal} from './modal';
export function showWinnerModal(fighter) {
  // call showModal function 
  const toWin = {
    bodyElement: fighter.name
  }
  showModal(toWin);
}
