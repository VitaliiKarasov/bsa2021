import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  function createInfo(keyValue) {
    const nameElement = createElement({ tagName: 'span', className: 'fighter-preview___property' });
    nameElement.innerText = keyValue, subStr => subStr.toUpperCase();
    nameElement.style = 'color: yellow; width: 600; display: grid'
  
    return nameElement;
  }

  function createFightersImage(source) {
    const attributes = { src: source };
    const imgElement = createElement({
      tagName: 'img',
      className: 'fighter-image___preview',
      attributes
    });

    if(position === 'right') {
      imgElement.style.transform = 'scale(-1, 1)';
    }
  
    return imgElement;
  }

  if(fighter) {
    const keyValueArray = Object.entries(fighter);
    fighterElement.append(createFightersImage(fighter['source']));
    const fightPropsContainer = createElement({ tagName: 'div', className: 'fighter-preview___props-box' });
    keyValueArray
    .filter(keyValueAll => keyValueAll[0] !== '_id' && keyValueAll[0] !== 'source')
    .forEach(keyValue => fightPropsContainer.append(createInfo(keyValue)));
    fighterElement.append(fightPropsContainer);
  }

  return fighterElement;
}



export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
